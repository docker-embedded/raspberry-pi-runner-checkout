# Author: Ari Mahpour
# Created: 03/04/2021
# Filename: gpio_test.py
# Description: Uses the RPi.GPIO library to test capabilities of toggling GPIOs
# Note: This will require a privileged Gitlab runner

try:
    import RPi.GPIO as GPIO
except:
    print("Warning: Raspberry Pi GPIO library will not be imported.")
    exit(1)

print("[STATUS]: Configuring GPIO")
GPIO.setmode(GPIO.BCM)
GPIO.setup(2, GPIO.OUT)

print("[STATUS]: Setting GPIO high")
GPIO.output(2, GPIO.HIGH)

print("[STATUS]: Setting GPIO low")
GPIO.output(2, GPIO.LOW)