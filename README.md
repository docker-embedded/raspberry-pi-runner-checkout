# Raspberry Pi Runner Checkout

This repository consists of various scripts that check out a Raspberry Pi's capability to run as a Gitlab runner. Use this repository after bringing up a Raspberry Pi for the first time.